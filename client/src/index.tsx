import React from "react";
import ReactDOM from "react-dom/client";
import io from "socket.io-client";
import App from "./App";

const main = async () => {
  const root = ReactDOM.createRoot(
    document.getElementById("root") as HTMLElement
  );
  root.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>
  );

  const TOKEN = localStorage.getItem("token");
    const resUser = await fetch(`http://localhost:8000/api/self`, {
      headers: {
        Authorization: `Bearer ${TOKEN}`,
      },
    });

    const user = await resUser.json();

    const socket = io("http://localhost:5000");
    socket.on("connect", () => {
      console.log("connected:", socket.id);
      socket.on("notification", (data) => {
        if (user.sendNotification) {
          if (user.notificationChannel === "log") {
            console.log(data);
          }
          if (user.notificationChannel === "alert") {
            alert(JSON.stringify(data));
          }
        }
      });
    });
  }
main()
