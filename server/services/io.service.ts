import { Server } from 'socket.io'
// import dotenv from 'dotenv'

// dotenv.config()

const WS_PORT: number = Number(process.env.WS_PORT) || 5000

class IOService {
  private static instance: IOService
  public readonly io: Server

  constructor () {
    this.io = new Server({
      cors: {
        origin: '*',
        credentials: true,
        methods: ['GET', 'POST', 'PUT', 'DELETE']
      }
    })
  }

    ws () {
    this.io.listen(WS_PORT)

    console.log(`WS server start: ${WS_PORT}`)

    this.io.on('connection', (socket) => {
      console.log('user connected', socket.id)

      socket.on('disconnect', () => {
        console.log('user disconnected')
      })
    })
  }

  public static getInstance (): IOService {
    if (!IOService.instance) {
      IOService.instance = new IOService()
    }
    return IOService.instance
  }
}
export default IOService.getInstance()
